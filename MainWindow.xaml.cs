﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace Microsoft.Samples.Kinect.BodyBasics
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using Excel = Microsoft.Office.Interop.Excel;


    
    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        /// <summary>
        /// Radius of drawn hand circles // Radio de circulos dibujados a mano
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Thickness of drawn joint lines // Grosor de las lineas articuladas dibujadas
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of clip edge rectangles // Grosor de los rectángulos de borde de clip.
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Constant for clamping Z values of camera space points from being negative // 
        /// Constante para sujetar los valores Z de los puntos de espacio de la cámara de ser negativos
        /// </summary>
        private const float InferredZPositionClamp = 0.1f;

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// Pincel utilizado para dibujar manos que actualmente se rastrean como cerradas
        /// </summary>
        private readonly Brush handClosedBrush = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// Pincel utilizado para dibujar manos que actualmente se rastrean como abiertas
        /// </summary>
        private readonly Brush handOpenBrush = new SolidColorBrush(Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// Pincel utilizado para dibujar las manos que se rastrean actualmente como en la posición de lazo (puntero)
        /// </summary>
        private readonly Brush handLassoBrush = new SolidColorBrush(Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// Pincel utilizado para dibujar uniones que actualmente se rastrean
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// Pincel usado para dibujar uniones que actualmente están inferidas.
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// Pluma utilizada para dibujar huesos que actualmente se infieren.
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Drawing group for body rendering output
        /// Grupo de dibujo para la salida de la representación del cuerpo.
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// Imagen de dibujo que mostraremos.
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// Asignador de coordenadas para asignar un tipo de punto a otro
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// definition of bones
        /// </summary>
        private List<Tuple<JointType, JointType>> bones;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// List of colors for each body tracked
        /// </summary>
        private List<Pen> bodyColors;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>

        List<double> Cabezax = new List<double>();
        List<double> CabezaY = new List<double>();
        List<double> EDorsalX = new List<double>();
        List<double> EDorsalY = new List<double>();
        List<double> EMediaX = new List<double>();
        List<double> EMediaY = new List<double>();
        List<double> HombroDerechoX = new List<double>();
        List<double> HombroDerechoY = new List<double>();
        List<double> HombroIzquierdoX = new List<double>();
        List<double> HombroIzquierdoY = new List<double>();
        List<double> CodoDerechoX = new List<double>();
        List<double> CodoDerechoY = new List<double>();
        List<double> CodoIzquierdoX = new List<double>();
        List<double> CodoIzquierdoY = new List<double>();
        List<double> ManoDerechaX = new List<double>();
        List<double> ManoDerechaY = new List<double>();
        List<double> ManoIzquierdaX = new List<double>();
        List<double> ManoIzquierdaY = new List<double>();
        List<string> Tiempo = new List<string>();
       

        double cabeza;


        public MainWindow()

        {
                       
            // one sensor is currently supported
            this.kinectSensor = KinectSensor.GetDefault();

            // get the coordinate mapper
            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            // get the depth (display) extents
            FrameDescription frameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;

            // get size of joint space
            this.displayWidth = frameDescription.Width;
            this.displayHeight = frameDescription.Height;

            // open the reader for the body frames
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            // a bone defined as a line between two joints
            this.bones = new List<Tuple<JointType, JointType>>();

           
            // Torso
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Head, JointType.Neck));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipLeft));

            // Right Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderRight, JointType.ElbowRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowRight, JointType.WristRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.HandRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandRight, JointType.HandTipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.ThumbRight));

            // Left Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderLeft, JointType.ElbowLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowLeft, JointType.WristLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.HandLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandLeft, JointType.HandTipLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.ThumbLeft));

            /*// Right Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipRight, JointType.KneeRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeRight, JointType.AnkleRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleRight, JointType.FootRight));

            // Left Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipLeft, JointType.KneeLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeLeft, JointType.AnkleLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleLeft, JointType.FootLeft));*/

            // populate body colors, one for each BodyIndex
            this.bodyColors = new List<Pen>();

            this.bodyColors.Add(new Pen(Brushes.Red, 6));
            this.bodyColors.Add(new Pen(Brushes.Orange, 6));
            this.bodyColors.Add(new Pen(Brushes.Green, 6));
            this.bodyColors.Add(new Pen(Brushes.Blue, 6));
            this.bodyColors.Add(new Pen(Brushes.Indigo, 6));
            this.bodyColors.Add(new Pen(Brushes.Violet, 6));

            // set IsAvailableChanged event notifier
            

            // initialize the components (controls) of the window
            this.InitializeComponent();
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute start up tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.FrameArrived += this.Reader_FrameArrived;
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
           

          /*  if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }*/
        }

        /// <summary>
        /// Handles the body frame data arriving from the sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_FrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            bool dataReceived = false;

            using (BodyFrame bodyFrame = e.FrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                }
            }

            if (dataReceived)
            {
                using (DrawingContext dc = this.drawingGroup.Open())
                {
                    // Draw a transparent background to set the render size
                    dc.DrawRectangle(Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                    int penIndex = 0;
                    foreach (Body body in this.bodies)
                    {
                        

          

                 
                        Pen drawPen = this.bodyColors[penIndex++];

                        // funcion donde puedo encontrar los datos.
 
                        if (body.IsTracked)
                        {

                            this.DrawClippedEdges(body, dc);

                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                            // convert the joint points to depth (display) space
                            Dictionary<JointType, Point> jointPoints = new Dictionary<JointType, Point>();

                            foreach (JointType jointType in joints.Keys)
                            {
                                // sometimes the depth(Z) of an inferred joint may show as negative
                                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                CameraSpacePoint position = joints[jointType].Position;
                                if (position.Z < 0)
                                {
                                    position.Z = InferredZPositionClamp;
                                }

                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                jointPoints[jointType] = new Point(depthSpacePoint.X, depthSpacePoint.Y);
                            }

                            this.DrawBody(joints, jointPoints, dc, drawPen);

                            this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                            this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);
                        }
                    }

                    // prevent drawing outside of our render area
                    this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                }
            }
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="drawingPen">specifies color to draw a specific body</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, DrawingContext drawingContext, Pen drawingPen)
        {
            // Draw the bones
            foreach (var bone in this.bones)
            {
                this.DrawBone(joints, jointPoints, bone.Item1, bone.Item2, drawingContext, drawingPen);
            }

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// /// <param name="drawingPen">specifies color to draw a specific bone</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext, Pen drawingPen)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = drawingPen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
          
            //*******************************************************************************//

            double RHandX = 0, RHandY = 0;
            double LHandX = 0, LHandY = 0;
            double SShoulderX = 0, SShoulderY = 0;
            double SMidX = 0, SMidY = 0;
            double HeadX = 0, HeadY = 0;
            double RShoulderX = 0, RShoulderY = 0;
            double LShoulderX = 0, LShoulderY = 0;
            double RElbowX = 0, RElbowY = 0;
            double LElbowX = 0, LElbowY = 0;

            double Hipo = 0;

            string PuntosCabeza = "";
            string PuntosManoDerecha = "";
            string PuntosManoIzquierda = "";
            string PuntosEspinaDorsal = "";
            string PuntosEspinaMedia = "";
            string PuntosHombroDerecho = "";
            string PuntosHombroIzquierdo = "";
            string PuntosCodoDerecho = "";
            string PuntosCodoIzquierdo = "";


            Point EspinaDorsal = new Point(0, 0);
            Point EspinaMedia = new Point(0, 0);

            FormattedText FormTextCabeza;
            FormattedText FormTextManoDerecha;
            FormattedText FormTextManoIzquierda;
            FormattedText FormTextEspinaDorsal;
            FormattedText FormTextEspinaMedia;
          //  FormattedText FormTextHombroDerecho;
          //  FormattedText FormTextHombroIzquierdo;
            FormattedText FormTextCodoDerecho;
            FormattedText FormTextCodoIzquierdo;


            if (jointType0.ToString().Equals("Head")) 
               {
                   HeadX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                   HeadX = Math.Round(HeadX, 5);
                   Cabezax.Add(HeadX);
                   cabeza = HeadX;
                   HeadY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                   HeadY = Math.Round(HeadY, 5);
                   CabezaY.Add(HeadY);
                   PuntosCabeza = "" + HeadX + "," + HeadY;
                   FormTextCabeza = new FormattedText(PuntosCabeza, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                   Point PECabeza = new Point(HeadX, HeadY);
                   drawingContext.DrawText(FormTextCabeza, PECabeza);
                   

               }

            if(jointType0.ToString().Equals("SpineShoulder")|| jointType1.ToString().Equals("SpineMid"))
               {

                   SShoulderX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                   SShoulderX = Math.Round(SShoulderX, 5);
                   EDorsalX.Add(SShoulderX);
                   SShoulderY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                   SShoulderY = Math.Round(SShoulderY, 5);
                   EDorsalY.Add(SShoulderY);
                   PuntosEspinaDorsal = "" + SShoulderX + "," + SShoulderY;
                   FormTextEspinaDorsal = new FormattedText(PuntosEspinaDorsal, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                   Point PEEspinaDorsal = new Point(SShoulderX, SShoulderY);
                   //drawingContext.DrawText(FormTextEspinaDorsal, PEEspinaDorsal);
                   EspinaDorsal.X = SShoulderX;
                   EspinaDorsal.Y = SShoulderY;


                   SMidX = Convert.ToDouble(jointPoints[jointType1].X.ToString());
                   SMidX = Math.Round(SMidX, 5);
                   EMediaX.Add(SMidX);
                   SMidY = Convert.ToDouble(jointPoints[jointType1].Y.ToString());
                   SMidY = Math.Round(SMidY, 5);
                   EMediaY.Add(SMidY);
                   PuntosEspinaMedia = "" + SMidX + "," + SMidY;
                   FormTextEspinaMedia = new FormattedText(PuntosEspinaMedia, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                   Point PEEspinaMedia = new Point(SMidX, SMidY);
                   drawingContext.DrawText(FormTextEspinaMedia, PEEspinaMedia);
                   EspinaMedia.X = SMidX;
                   EspinaMedia.Y = SMidY;
                   //caja.Text = Convert.ToString(SMidX);

                 //  CameraSpacePoint posSMid = joints[jointType1].Position;
            
            }

          if (jointType0.ToString().Equals("ShoulderRight")) 
                {
                    RShoulderX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                    RShoulderX = Math.Round(RShoulderX, 5);
                    HombroDerechoX.Add(RShoulderX);
                    RShoulderY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                    RShoulderY = Math.Round(RShoulderY, 5);
                    HombroDerechoY.Add(RShoulderY);
                    PuntosHombroDerecho = "" + RShoulderX + "," + RShoulderY;
                    
                    //FormTextHombroDerecho = new FormattedText(PuntosHombroDerecho, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                    //Point PEHombroDerecho = new Point(RShoulderX, RShoulderY);
                    //drawingContext.DrawText(FormTextHombroDerecho, PEHombroDerecho);
                
                
                }

            if (jointType0.ToString().Equals("ElbowRight"))
               {
                   RElbowX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                   RElbowX = Math.Round(RElbowX, 5);
                   CodoDerechoX.Add(RElbowX);
                   RElbowY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                   RElbowY = Math.Round(RElbowY, 5);
                   CodoDerechoY.Add(RElbowY);
                   PuntosCodoDerecho = "" + RElbowX + "," + RElbowY;
                   FormTextCodoDerecho = new FormattedText(PuntosCodoDerecho, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                   Point PECodoDerecho = new Point(RElbowX, RElbowY);
                   drawingContext.DrawText(FormTextCodoDerecho, PECodoDerecho);
            
               }
            if (jointType0.ToString().Equals("HandRight"))
            {
                RHandX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                RHandX = Math.Round(RHandX, 5);
                ManoDerechaX.Add(RHandX);
                RHandY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                RHandY = Math.Round(RHandY, 5);
                ManoDerechaY.Add(RHandY);
                PuntosManoDerecha = "" + RHandX + "," + RHandY;
                FormTextManoDerecha = new FormattedText(PuntosManoDerecha, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                Point PEManoDerecha = new Point(RHandX, RHandY);
                drawingContext.DrawText(FormTextManoDerecha, PEManoDerecha);
            }

            if (jointType0.ToString().Equals("ShoulderLeft"))
            {
                LShoulderX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                LShoulderX = Math.Round(LShoulderX, 5);
                HombroIzquierdoX.Add(LShoulderX);
                LShoulderY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                LShoulderY = Math.Round(LShoulderY, 5);
                HombroIzquierdoY.Add(LShoulderY);
                PuntosHombroIzquierdo = "" + LShoulderX + "," + LShoulderY;
               // FormTextHombroIzquierdo = new FormattedText(PuntosHombroIzquierdo, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
               // Point PEHombroIzquierdo = new Point(LShoulderX, LShoulderY);
               // drawingContext.DrawText(FormTextHombroIzquierdo, PEHombroIzquierdo);


            }

            if (jointType0.ToString().Equals("ElbowLeft"))
            {
                LElbowX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                LElbowX = Math.Round(LElbowX, 5);
                CodoIzquierdoX.Add(LElbowX);
                LElbowY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                LElbowY = Math.Round(LElbowY, 5);
                CodoIzquierdoY.Add(LElbowY);
                PuntosCodoIzquierdo = "" + LElbowX + "," + LElbowY;
                FormTextCodoIzquierdo = new FormattedText(PuntosCodoIzquierdo, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                Point PECodoIzquierdo = new Point(LElbowX, LElbowY);
                drawingContext.DrawText(FormTextCodoIzquierdo, PECodoIzquierdo);

            }

           
            if (jointType0.ToString().Equals("HandLeft"))
            {
                LHandX = Convert.ToDouble(jointPoints[jointType0].X.ToString());
                LHandX = Math.Round(LHandX, 5);
                ManoIzquierdaX.Add(LHandX);
                LHandY = Convert.ToDouble(jointPoints[jointType0].Y.ToString());
                LHandY = Math.Round(LHandY, 5);
                ManoIzquierdaY.Add(LHandY);
                Tiempo.Add(DateTime.Now.ToLongTimeString());
                PuntosManoIzquierda = "" + LHandX + "," + LHandY;
                FormTextManoIzquierda = new FormattedText(PuntosManoIzquierda, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface("Verdana"), 10, Brushes.White);
                Point PEManoIzquierda = new Point(LHandX, LHandY);
                drawingContext.DrawText(FormTextManoIzquierda, PEManoIzquierda);

            }





        }

        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);
                    break;
            }
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping body data
        /// </summary>
        /// <param name="body">body to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawClippedEdges(Body body, DrawingContext drawingContext)
        {
            FrameEdges clippedEdges = body.ClippedEdges;

            if (clippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, this.displayHeight - ClipBoundsThickness, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, this.displayHeight));
            }

            if (clippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(this.displayWidth - ClipBoundsThickness, 0, ClipBoundsThickness, this.displayHeight));
            }
        }

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            // on failure, set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }

        private void Guardar_Click(object sender, RoutedEventArgs e)
        {
            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

           
           var excelApp = new Excel.Application();
            
            excelApp.Workbooks.Add();
            Excel._Worksheet workSheet = (Excel.Worksheet)excelApp.ActiveSheet;
           
            
            workSheet.Cells[1, "A"] = "CabezaX";
            workSheet.Cells[1, "B"] = "CabezaY";
            workSheet.Cells[1, "C"] = "E.DorsalX";
            workSheet.Cells[1, "D"] = "E.DorsalY";
            workSheet.Cells[1, "E"] = "E. MediaX";
            workSheet.Cells[1, "F"] = "E. MediaY";
            workSheet.Cells[1, "G"] = "Hombro DerechoX";
            workSheet.Cells[1, "H"] = "Hombro DerechoY";
            workSheet.Cells[1, "I"] = "Hombro IzquierdoX";
            workSheet.Cells[1, "J"] = "Hombro IzquierdoY";
            workSheet.Cells[1, "K"] = "Codo DerechoX";
            workSheet.Cells[1, "L"] = "Codo DerechoY";
            workSheet.Cells[1, "M"] = "Codo IzquierdoX";
            workSheet.Cells[1, "N"] = "Codo IzquierdoY";
            workSheet.Cells[1, "O"] = "Mano DerechaX";
            workSheet.Cells[1, "P"] = "Mano DerechaY";
            workSheet.Cells[1, "Q"] = "Mano IzquierdaX";
            workSheet.Cells[1, "R"] = "Mano IzquierdaY";
            workSheet.Cells[1, "S"] = "Tiempo";

          
          
            for (int h = 0; h < Cabezax.Count; h++)
            {
                workSheet.Cells[h + 2, "A"] = Cabezax[h];
                workSheet.Cells[h + 2, "B"] = CabezaY[h];
                workSheet.Cells[h + 2, "C"] = EDorsalX[h];
                workSheet.Cells[h + 2, "D"] = EDorsalY[h];
                workSheet.Cells[h + 2, "E"] = EMediaX[h];
                workSheet.Cells[h + 2, "F"] = EMediaY[h];
                workSheet.Cells[h + 2, "G"] = HombroDerechoX[h];
                workSheet.Cells[h + 2, "H"] = HombroDerechoY[h];
                workSheet.Cells[h + 2, "I"] = HombroIzquierdoX[h];
                workSheet.Cells[h + 2, "J"] = HombroIzquierdoY[h];
                workSheet.Cells[h + 2, "K"] = CodoDerechoX[h];
                workSheet.Cells[h + 2, "L"] = CodoDerechoY[h];
                workSheet.Cells[h + 2, "M"] = CodoIzquierdoX[h];
                workSheet.Cells[h + 2, "N"] = CodoIzquierdoY[h];
                workSheet.Cells[h + 2, "O"] = ManoDerechaX[h];
                workSheet.Cells[h + 2, "P"] = ManoDerechaY[h];
                workSheet.Cells[h + 2, "Q"] = ManoIzquierdaX[h];
                workSheet.Cells[h + 2, "R"] = ManoIzquierdaY[h];
                workSheet.Cells[h + 2, "S"] = Tiempo[h];
                         
               
           
            }
            string carpeta="";
            Microsoft.Win32.SaveFileDialog salvar = new Microsoft.Win32.SaveFileDialog();
            salvar.FileName = "Libro1";
            salvar.DefaultExt = ".xlsx";
            salvar.Filter = "Archivos xlsx(*.xlsx)|*.xlsx";
            salvar.Title = "Guardar";

            Nullable<bool> result = salvar.ShowDialog();


            if (result == true)
            {
                carpeta = salvar.FileName;
                //  excelApp.Visible = true;



            }
            workSheet.SaveAs(carpeta);
            System.Windows.MessageBox.Show("Todos sus datos se han guardado con exito");
            excelApp.Visible = true;

            if (this.bodyFrameReader != null)
            {
                // BodyFrameReader is IDisposable
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }
            

            
           
        }

        private void conexion_Click(object sender, RoutedEventArgs e)
        {
            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            // open the sensor
            this.kinectSensor.Open();

            // set the status text
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            // use the window object as the view model in this simple example
            this.DataContext = this;
        }

       
    }
}
